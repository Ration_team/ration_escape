package com.escape.ration.escapeapp.activity.main;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.escape.ration.escapeapp.R;
import com.escape.ration.escapeapp.base.BaseActivity;

public class MainActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
}
