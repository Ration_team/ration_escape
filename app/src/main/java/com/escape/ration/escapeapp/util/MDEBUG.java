package com.escape.ration.escapeapp.util;

import android.util.Log;

import com.escape.ration.escapeapp.BuildConfig;

/**
 * User: Marty
 * Date: 2018-05-30
 * Time: 오후 6:34
 * Description:
 */
public class MDEBUG {

    static boolean FLAG_MODE = BuildConfig.BUILD_TYPE.equals("debug");

    public static void debug (String msg){
        if (FLAG_MODE)
            Log.d("<Debug>>>>",buildLogMsg(msg));
    }

    public static String buildLogMsg(String message) {

        StackTraceElement ste = Thread.currentThread().getStackTrace()[4];

        StringBuilder sb = new StringBuilder();

        sb.append("[");
        sb.append(ste.getFileName().replace(".java", ""));
        sb.append("::");
        sb.append(ste.getMethodName());
        sb.append("]");
        sb.append(message);

        return sb.toString();

    }

}

