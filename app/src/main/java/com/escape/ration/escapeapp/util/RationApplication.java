package com.escape.ration.escapeapp.util;

import android.app.Application;

/**
 * User: Marty
 * Date: 2018-05-30
 * Time: 오후 6:29
 * Description:
 */
public class RationApplication extends Application {

    private static RationApplication application;

    public static RationApplication getAPP(){
        return application;
    }
    @Override
    public void onCreate() {
        super.onCreate();
        application = this;
    }
}
