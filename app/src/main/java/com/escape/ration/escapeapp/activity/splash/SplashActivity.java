package com.escape.ration.escapeapp.activity.splash;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.escape.ration.escapeapp.R;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
    }
}
